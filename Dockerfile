FROM alpine:latest

MAINTAINER Rigel Kent <par@rigelk.eu>

# Build-time metadata as defined at http://label-schema.org
ARG VCS_REF
ARG VCS_URL
ARG BUILD_DATE
ARG VERSION=v0.9.7
LABEL org.label-schema.vcs-ref=$VCS_REF \
      org.label-schema.vcs-url=$VCS_URL \
      org.label-schema.build-date=$BUILD_DATE \
      org.label-schema.version=$VERSION \
      org.label-schema.schema-version="1.0"

RUN echo "@testing http://nl.alpinelinux.org/alpine/edge/testing" >>/etc/apk/repositories

# python is necessary for ssdb-cli
RUN apk update && \
    apk add gcc python && \
    apk add --virtual .build-deps autoconf make g++ linux-headers git && \
    apk add ca-certificates && \
    update-ca-certificates && \
    apk add openssl

RUN apk add --update --no-cache zlib zlib-dev bzip2 bzip2-dev

RUN mkdir -p /usr/src/ardb

ARG storage_engine=rocksdb
ENV storage_engine=$storage_engine
RUN git clone -b "$VERSION" --single-branch --depth 1 https://github.com/yinqiwen/ardb.git /usr/src/ardb && \
    make -C /usr/src/ardb

RUN apk del .build-deps

RUN mkdir /ardb_home

RUN sed -e 's@home .*@home /ardb_home@' -i /usr/src/ardb/ardb.conf

EXPOSE 16379
VOLUME /ardb_home
WORKDIR /ardb_home

CMD ["/usr/src/ardb/src/ardb-server", "/usr/src/ardb/ardb.conf"]

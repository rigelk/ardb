# ARDB docker image

Ardb is a BSD licensed, redis-protocol compatible persistent nosql, it support multiple storage engines as backend like [Google's LevelDB](https://github.com/google/leveldb), [Facebook's RocksDB](https://github.com/facebook/rocksdb), [OpenLDAP's LMDB](http://symas.com/mdb/), [WiredTiger](http://www.wiredtiger.com/), [PerconaFT](https://github.com/percona/PerconaFT),[Couchbase's ForestDB](https://github.com/couchbase/forestdb) the default backend is [Facebook's RocksDB](https://github.com/facebook/rocksdb).

This is a Dockerfile for it and associated [published docker images](https://hub.docker.com/r/rigelk/ardb).

## Building

```bash
make
```

or

```bash
docker build . -t $USER/ardb --build-arg storage_engine=lmdb # defaults to rocksdb
```

## Installation

```bash
docker pull rigelk/ardb
```

## Usage

```bash
docker run -d rigelk/ardb
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[Unlicense](https://choosealicense.com/licenses/unlicense/) - see LICENSE file
